<?php

namespace Drupal\element451_api;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

/**
 * ApiCall service.
 */
class Element451Api {

  use StringTranslationTrait;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * The cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $logger;

  /**
   * The config for Element451.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Debugging enabled.
   *
   * @var bool
   */
  protected bool $debug;

  /**
   * Configuration complete.
   *
   * @var bool
   */
  public bool $configurationComplete = FALSE;

  /**
   * Constructs an ApiCall object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ClientInterface $http_client, CacheBackendInterface $cache, MessengerInterface $messenger, LoggerChannelFactoryInterface $logger) {
    $this->configFactory = $config_factory;
    $this->httpClient = $http_client;
    $this->cache = $cache;
    $this->messenger = $messenger;
    $this->logger = $logger;
    $this->config = $this->configFactory->get('element451_api.settings');
    $this->debug = $this->config->get('debug');
    $this->configurationComplete = $this->isConfigurationComplete();
  }

  /**
   * Check if all required configuration values are set.
   *
   * @return bool
   *   TRUE if all fields are set.
   */
  public function isConfigurationComplete() {
    $required_fields = [
      'email',
      'password',
      'client',
      'api_host',
      'access_token',
      'analytics_token',
      'feature_token',
    ];
    foreach ($required_fields as $field) {
      if (!$this->config->get($field)) {
        return FALSE;
      }
    } // Loop thru required fields.
    return TRUE;
  }

  /**
   * Calls the chosen request using the parameters given.
   *
   * @param string $method
   *   The chosen request.
   * @param string $url
   *   The complete URL for the request.
   * @param array $headers
   *   The headers to use for the request.
   * @param string $body
   *   The body to use for the request.
   * @param array $options
   *   Any additional headers you want to use.
   *
   * @return false|mixed|string
   *   A varying value based on if the request succeeded.
   */
  public function call(string $method, string $url, array $headers = [], string $body = '', array $options = []) {

    if (!$this->configurationComplete) {
      $this->logger->get('element451_api')->error('Element451 API configuration values not set.', ['link' => Link::createFromRoute($this->t('Settings'), 'element451_api.settings_form')->toString()]);
      return FALSE;
    }

    $request_options = $options + [
      'headers' => $headers + [
        'Content-Type' => 'application/json',
      ],
      'auth' => [$this->config->get('email'), $this->config->get('password')],
      'connect_timeout' => 0,
      'read_timeout' => 0,
      'timeout' => 0,
    ];

    if ($body) {
      $request_options['body'] = $body;
    }

    if ($this->debug) {
      $message = 'Element451 API call. <br>Method: @method <br>URL: @url <br>Headers: <pre>@headers</pre> <br>Body: <pre>@body</pre>';
      $args = [
        '@method' => $method,
        '@url' => $url,
        '@headers' => print_r($request_options['headers'], TRUE),
        '@body' => print_r($body, TRUE),
      ];
      $this->logger->get('element451_api')->debug($message, $args);

    }

    $response = NULL;
    $data = NULL;
    try {
      $response = $this->httpClient->request($method, $url, $request_options);
      if ($response_body = $response->getBody()) {
        if ($data = $response_body->getContents()) {
          $data = json_decode($data);
        }
      }
      if ($this->debug) {
        $message = 'Element451 API call. <br>Request options:<pre>@request_options</pre> <br>Response:<pre>@result</pre> <br>Data:<pre>@data</pre>';
        $args = [
          '@request_options' => print_r($request_options, TRUE),
          '@result' => print_r($response, TRUE),
          '@data' => print_r($data, TRUE),
        ];
        $this->logger->get('element451_api')->debug($message, $args);
      }
    }
    catch (RequestException | GuzzleException $e) {
      watchdog_exception('element451_api', $e);
    }

    if (!empty($data)) {
      return $data;
    }

    $message = 'Error calling Element451 API. Request options: <pre>@request_options</pre> <br>Result:<pre>@result</pre>';
    $args = [
      '@request_options' => print_r($request_options, TRUE),
      '@result' => print_r($response, TRUE),
    ];
    $this->logger->get('element451_api')->error($message, $args);

    return FALSE;

  }

  /**
   * Replace tokens in URL.
   *
   * @param string $url
   *   Request URL.
   * @param array $params
   *   Replacement parameters.
   *
   * @return string
   *   The complete URL for the request.
   */
  public function buildUrl(string $url, array $params = []) {
    $params += [
      '{{client}}' => $this->config->get('client'),
      '{{api}}' => $this->config->get('api_host'),
      '{{token}}' => urlencode($this->config->get('access_token')),
      '{{analytics}}' => urlencode($this->config->get('analytics_token')),
      '{{feature}}' => urlencode($this->config->get('feature_token')),
    ];
    return strtr($url, $params);
  }

  /**
   * Creates an event on Element451 using a given array of data.
   *
   * @param array $data
   *   The array of data to be used to create the event.
   *
   * @return string
   *   The guid of the created event or an empty string if not created.
   */
  public function createEvent(array $data) {

    $headers = [
      'Accept' => 'application/json, text/plain, */*',
    ];

    $body = json_encode($data);
    $guid = '';

    if ($this->debug) {
      $message = 'Calling Element451Api::createEvent() with data: <br><pre>@data</pre>';
      $args = [
        '@data' => print_r($data, TRUE),
      ];
      $this->logger->get('element451_api')->debug($message, $args);
    }

    $url = 'https://{{client}}.{{api}}/v2/events/partials?token={{token}}&analytics={{analytics}}&feature={{feature}}';
    if ($response = $this->call('POST', $this->buildUrl($url), $headers, $body)) {
      $guid = $response->data ?? '';
    }

    if ($guid) {
      // Update event needs to be called to set the published status,
      // event dates, and image.
      $this->updateEvent($guid, $data);
    }

    return $guid;

  }

  /**
   * Updates a chosen event on Element451.
   *
   * @param string $guid
   *   The guid of the event to be updated.
   * @param array $data
   *   The updated data for the event.
   */
  public function updateEvent(string $guid, array $data) {

    $body = json_encode($data);

    if ($this->debug) {
      $message = 'Calling Element451Api::updateEvent(%guid) with data: <br><pre>@data</pre>';
      $args = [
        '%guid' => $guid,
        '@data' => print_r($data, TRUE),
      ];
      $this->logger->get('element451_api')->debug($message, $args);
    }

    $done = FALSE;
    $url = 'https://{{client}}.{{api}}/v2/events/{{EVENT_GUID}}?token={{token}}&analytics={{analytics}}&feature={{feature}}';
    if ($response = $this->call('PUT', $this->buildUrl($url, ['{{EVENT_GUID}}' => $guid]), [], $body)) {
      $done = $response->data->done ?? FALSE;
    }

    if (!$done) {
      return FALSE;
    }

    if (!$this->addEventDate($guid, $data)) {
      return FALSE;
    }

    if (!empty($data['image'])) {
      if (!$this->uploadImage($guid, $data['image'])) {
        return FALSE;
      }
    }

    return $done;

  }

  /**
   * Deletes a chosen event from Element451.
   *
   * @param string $guid
   *   The guid of the event to be deleted.
   */
  public function deleteEvent(string $guid) {

    if ($this->debug) {
      $message = 'Calling Element451Api::deleteEvent(%guid)';
      $args = [
        '%guid' => $guid,
      ];
      $this->logger->get('element451_api')->debug($message, $args);
    }
    $done = FALSE;
    $url = 'https://{{client}}.{{api}}/v2/events/{{EVENT_GUID}}?analytics={{analytics}}&feature={{feature}}';
    if ($response = $this->call('DELETE', $this->buildUrl($url, ['{{EVENT_GUID}}' => $guid]))) {
      $done = $response->data ?? FALSE;
    }
    if (!$done) {
      $args = [
        '%guid' => $guid,
      ];
      $this->logger->get('element451_api')->error('Delete on Element451 failed on %guid', $args);
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Adds the start and end date to a chosen event on Element451.
   *
   * @param string $guid
   *   The guid of the chosen event.
   * @param array $data
   *   The data containing the dates of the event.
   *
   * @return bool
   *   A boolean based on if it succeeded.
   */
  public function addEventDate(string $guid, array $data) {

    $body = json_encode($data);

    if ($this->debug) {
      $message = 'Calling Element451Api::addEventDate(%guid) with data: <br><pre>@data</pre>';
      $args = [
        '%guid' => $guid,
        '@data' => print_r($data, TRUE),
      ];
      $this->logger->get('element451_api')->debug($message, $args);
    }

    $done = FALSE;
    $url = 'https://{{client}}.{{api}}/v2/events/date/{{EVENT_GUID}}?token={{token}}&analytics={{analytics}}&feature={{feature}}';

    if ($response = $this->call('POST', $this->buildUrl($url, ['{{EVENT_GUID}}' => $guid]), [], $body)) {
      $done = $response->data->done ?? FALSE;
    }

    return $done;

  }

  /**
   * Uploads an image to Element451 and calls a method to add it to an event.
   *
   * @param string $guid
   *   The guid of the event.
   * @param array $image
   *   An array containing the necessary information about the image.
   *
   * @return string|false
   *   The guid of the uploaded image.
   */
  public function uploadImage(string $guid, array $image) {

    if ($this->debug) {
      $message = 'Calling Element451Api::uploadImage(%guid) with image: <br><pre>@image</pre>';
      $args = [
        '%guid' => $guid,
        '@image' => print_r($image, TRUE),
      ];
      $this->logger->get('element451_api')->debug($message, $args);
    }

    $boundary = '-----' . md5($image['uri']);
    $headers = [
      'Content-Type' => 'multipart/form-data; boundary=' . $boundary,
    ];

    if (!$image_contents = file_get_contents($image['uri'])) {
      $message = 'Image at %uri could not be read.';
      $args = [
        '%uri' => $image['uri'],
      ];
      $this->logger->get('element451_api')->error($message, $args);
      return FALSE;
    }
    $image_name = str_replace('"', '', basename($image['uri']));

    $body = <<<EOT
--{$boundary}
Content-Disposition: form-data; name="file"; filename="{$image_name}"
Content-Type: {$image['mime_type']}

{$image_contents}
--{$boundary}--

EOT;

    $url = 'https://{{client}}.{{api}}/v2/media/all?analytics={{analytics}}&feature={{feature}}';

    $response = $this->call('POST', $this->buildUrl($url), $headers, $body);
    $image_guid = $response->data->guid ?? NULL;

    if ($image_guid) {
      return $this->addImageToEvent($guid, $image_guid);
    }

    return FALSE;

  }

  /**
   * Adds a chosen image to an event.
   *
   * @param string $guid
   *   The guid of the event.
   * @param string $image_guid
   *   The guid of the image.
   *
   * @return string|false
   *   The image guid or false on failure.
   */
  public function addImageToEvent(string $guid, string $image_guid) {

    if ($this->debug) {
      $message = 'Calling Element451Api::addImageToEvent(%guid, %image_guid)';
      $args = [
        '%guid' => $guid,
        '%image_guid' => $image_guid,
      ];
      $this->logger->get('element451_api')->debug($message, $args);
    }

    $boundary = '-----' . md5($image_guid);
    $headers = [
      'Content-Type' => 'multipart/form-data; boundary=' . $boundary,
    ];

    $body = <<<EOT
--{$boundary}
Content-Disposition: form-data; name="imageGuid"

{$image_guid}
--{$boundary}--

EOT;

    $url = 'https://{{client}}.{{api}}/v2/events/image/{{EVENT_GUID}}?analytics={{analytics}}&feature={{feature}}';
    $response = $this->call('POST', $this->buildUrl($url, ['{{EVENT_GUID}}' => $guid]), $headers, $body);

    return $response->data->guid ?? FALSE;
  }

  /**
   * Lists events based on a given query array.
   *
   * @param array $query
   *   The query array.
   *
   * @return array
   *   The list of events.
   */
  public function listEvents(array $query = []) {
    if ($this->debug) {
      $message = 'Calling Element451Api::listEvents().';
      $this->logger->get('element451_api')->debug($message);
    }

    $url = 'https://{{client}}.{{api}}/v2/events/list?analytics={{analytics}}&feature={{feature}}';
    if ($query) {
      $query_string = UrlHelper::buildQuery($query);
      $url .= '&' . $query_string;
    }
    $response = $this->call('GET', $this->buildUrl($url));

    return $response->data ?? [];

  }

  /**
   * Gets a list of applications.
   *
   * @return array
   *   The list of applications.
   */
  public function getApplications() {

    if ($this->debug) {
      $message = 'Calling Element451Api::getApplications().';
      $this->logger->get('element451_api')->debug($message);
    }

    $url = 'https://{{client}}.{{api}}/v2/applications?embed[list]=1&token={{token}}&analytics={{analytics}}&feature={{feature}}';

    $response = $this->call('GET', $this->buildUrl($url));

    return $response->data ?? [];

  }

  /**
   * Retrieves a specific event.
   *
   * @param string $guid
   *   The guid of the chosen event.
   *
   * @return array
   *   The chosen event.
   */
  public function getEvent(string $guid) {

    if ($this->debug) {
      $message = 'Calling Element451Api::getEvent(%guid).';
      $args = [
        '%guid' => $guid,
      ];
      $this->logger->get('element451_api')->debug($message, $args);
    }

    $url = 'https://{{client}}.{{api}}/v2/events/list/{{EVENT_GUID}}?analytics={{analytics}}&feature={{feature}}';
    $response = $this->call('GET', $this->buildUrl($url, ['{{EVENT_GUID}}' => $guid]));

    return $response->data ?? [];

  }

  /**
   * Lists the categories for events.
   *
   * @return array
   *   The categories.
   */
  public function listEventsCategories() {
    if ($this->debug) {
      $message = 'Calling Element451Api::listEventsCategories().';
      $this->logger->get('element451_api')->debug($message);
    }
    $url = 'https://{{client}}.{{api}}/v2/events/category?analytics={{analytics}}&feature={{feature}}';
    $response = $this->call('GET', $this->buildUrl($url));

    return $response->data ?? [];

  }

}

<?php

namespace Drupal\element451_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Element 451 API settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'element451_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['element451_api.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('element451_api.settings');

    $form['environment_variables'] = [
      '#type' => 'details',
      '#title' => $this->t('Environment Variables'),
      '#open' => TRUE,
    ];
    $form['environment_variables']['info'] = [
      '#type' => 'markup',
      '#markup' => '<div class="description">'
      . $this->t('Values of <a href="@url" target="_blank">Environment Variables</a> defined in the Element451 documentation. The Element451 support team will provide these values to you.', ['@url' => 'https://api.element451.com/#intro'])
      . '</div>',
      '#open' => TRUE,
    ];
    $form['environment_variables']['api_host'] = [
      '#type' => 'radios',
      '#title' => $this->t('API Host'),
      '#options' => [
        'development.dev.451.io' => $this->t('Test (development.dev.451.io)'),
        'api.451.io' => $this->t('Production (api.451.io)'),
      ],
      '#description' => $this->t('URL of the API to access.'),
      '#default_value' => $config->get('api_host'),
      '#required' => TRUE,
    ];
    $form['environment_variables']['client'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client'),
      '#description' => $this->t('Client token (subdomain) which is the unique client identifier.'),
      '#default_value' => $config->get('client'),
      '#required' => TRUE,
    ];
    $form['environment_variables']['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Token'),
      '#default_value' => $config->get('access_token'),
      '#required' => TRUE,
    ];
    $form['environment_variables']['analytics_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Analytics Token'),
      '#default_value' => $config->get('analytics_token'),
      '#required' => TRUE,
    ];
    $form['environment_variables']['feature_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Feature Token'),
      '#default_value' => $config->get('feature_token'),
      '#required' => TRUE,
    ];

    $form['login_info'] = [
      '#type' => 'details',
      '#title' => $this->t('Login information'),
      '#open' => TRUE,
    ];
    $form['login_info']['description'] = [
      '#type' => 'markup',
      '#markup' => '<div class="description">'
      . $this->t('Your login information for the Element451 site.')
      . '</div>',
      '#open' => TRUE,
    ];
    $form['login_info']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#default_value' => $config->get('email'),
      '#required' => TRUE,
    ];
    $form['login_info']['password'] = [
      '#type' => 'password',
      '#title' => $this->t('Password'),
      '#description' => $this->t('Only required if changing the password.'),
      '#default_value' => '',
      '#required' => FALSE,
    ];
    if (!$config->get('password')) {
      $form['login_info']['password']['#required'] = TRUE;
      $form['login_info']['password']['#description'] = '';
    }

    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug'),
      '#description' => $this->t('If checked debug messages will be written to the watchdog log'),
      '#default_value' => $config->get('debug'),
    ];

    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('element451_api.settings');

    $config->set('api_host', $form_state->getValue('api_host'));
    $config->set('client', $form_state->getValue('client'));
    $config->set('access_token', $form_state->getValue('access_token'));
    $config->set('analytics_token', $form_state->getValue('analytics_token'));
    $config->set('feature_token', $form_state->getValue('feature_token'));
    $config->set('email', $form_state->getValue('email'));
    if (!empty($form_state->getValue('password'))) {
      $config->set('password', $form_state->getValue('password'));
    }
    $config->set('debug', $form_state->getValue('debug'));

    $config->save();
    parent::submitForm($form, $form_state);
  }

}

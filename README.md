# Element 451 API

Drupal implementation of the Element 451 API documented here:

https://api.element451.com/

Note that this documentation is incomplete and is missing some
required calls.

This module is **for developers only** as it will require PHP code to
send and receive Element 451 data.

## Supported API calls

This module mainly implements the calls related to Events, but could be
updated to work with other Element 451 API calls.

Supported API calls:

1. List Events
2. Get Event Details
3. Create Event
4. Update Event
5. Add Dates to Event
6. Upload File
7. Add Image to Event
8. List Events Categories
9. Get Applications

### Notes

1. When calling "Create Event", `categories` is a required field.
2. When calling "Create Event", it's not possible to set the
`published_status`. That must be done separately in an "Update Event"
call. The API call to `Element451Api::createEvent()` does this
automatically.
3. When calling "Create Event", `attendee_limit` must be in
the `$data` array. It can be set to '' if it is not needed.

## Installation and configuration

Install the Element 451 API module normally. After installation, you
will need to save the API settings under
Configuration > System > Element 451 API:

https://example.com/admin/config/system/element451-api

Note that the password will be stored in configuration.

You may prefer to add the password (and, possibly email) to your
`settings.local.php` file like this:

```php
$config['element451_api.settings']['email'] = 'email@example.com';
$config['element451_api.settings']['password'] = 'YourPassword';
```

## Example implementation with Drupal site

This module could be used to sync Drupal events to Element 451. In
that case, you would have your own implementation of entity hooks to
send data to Element 451 when Drupal events are created, updated, or
deleted.

When creating and updating Drupal events, an array of event data will
need to be created and sent to Element 451 via these API calls.

### Element 451 GUID field probably needed

You will most-likely want to add an Element 451 GUID field
(e.g. `field_element451_guid`) to the event node so that you will be
able to update Element 451 events when events are updated on the
Drupal site.

### Create data array from event nodes

```php
/**
 * Creates a data array from a given event for sending to Element 451.
 *
 * @param Drupal\Core\Entity\EntityInterface $entity
 *   The entity that represents the event.
 *
 * @return array
 *   The data array.
 */
function example_create_element451_api_event_data(EntityInterface $entity) {

  // If we have a featured image, add that to the data array.
  $image = [];
  if (!empty($entity->field_feature_event_image->entity)) {
    $image = [
      'uri' => $entity->field_feature_event_image->entity->getFileUri(),
      'mime_type' => $entity->field_feature_event_image->entity->getMimeType(),
    ];
  }

  // Set the start/end dates.
  $l_tz = new \DateTimeZone(date_default_timezone_get());
  $start_date = $entity->field_event_date->date;
  $start_date->setTimeZone($l_tz);
  if (!empty($entity->field_event_end_date->date)) {
    $end_date = $entity->field_event_end_date->date;
    $end_date->setTimeZone($l_tz);
  }
  else {
    // If no end date is set, default to 1 hour after start time.
    $end_date = clone $start_date;
    $interval = new \DateInterval('PT1H');
    $end_date->add($interval);
  }

  $data = [
    'item' => [
      'title' => $entity->label(),
      'privacy' => 'public',
      'publishing_status' => $entity->isPublished() ? 'published' : 'draft',
      // We hard-coded the event type.
      'event_type' => 'information-sessions',
      // The categories field is required!
      'categories' => ['admissions'],
      'role' => 'parent',
      'active' => TRUE,
      'type' => 'simple',
      'allday' => FALSE,
      // This needs to be set or it won't display correctly. Not sure why!
      'attendee_limit' => '',
      'date' => [
        'start_date' => $start_date->format('Y-m-d'),
        'start_time' => $start_date->format('H:i:s'),
        'end_date' => $end_date->format('Y-m-d'),
        'end_time' => $end_date->format('H:i:s'),
        'timezone' => $l_tz->getName(),
      ],
    ],
    'image' => $image,
  ];

  return $data;

}
```

### Send event to Element 451 on event node creation

```php

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function example_node_insert(EntityInterface $entity) {

  if ($entity->bundle() != 'event') {
    return;
  }

  /** @var \Drupal\element451_api\Element451Api $api */
  $api = \Drupal::service('element451_api.api_call');

  $data = example_create_element451_api_event_data($entity);
  if (!$data) {
    return;
  }

  // Add the Element 451 GUID to the node so we can use it later.
  $entity->field_element451_guid->value = $api->createEvent($data);
  $entity->skip_element451_api_update = TRUE;
  $entity->save();

}
```

### Update event on Element 451 when event node is updated

```php
/**
 * Implements hook_ENTITY_TYPE_update().
 */
function example_node_update(EntityInterface $entity) {

  if ($entity->bundle() != 'event') {
    return;
  }
  if (!empty($entity->skip_element451_api_update)) {
    return;
  }

  /** @var \Drupal\element451_api\Element451Api $api */
  $api = \Drupal::service('element451_api.api_call');

  $data = example_create_element451_api_event_data($entity);

  if (empty($entity->field_element451_guid->value)) {
    $entity->field_element451_guid->value = $api->createEvent($data);
    $entity->skip_element451_api_update = TRUE;
    $entity->save();
  }
  else {
    $api->updateEvent($entity->field_element451_guid->value, $data);
  }

}
```

### Delete event on Element 451 when event node is deleted

```php
/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function example_node_delete(EntityInterface $entity) {
  if ($entity->bundle() != 'event' || empty($entity->field_element451_guid)) {
    return;
  }
  /** @var \Drupal\element451_api\Element451Api $api */
  $api = \Drupal::service('element451_api.api_call');
  $api->deleteEvent($entity->field_element451_guid->value);
}
```
